<?php 
// =============================
// !!! УДАЛИТЬ ТЕКСТ LOREMIPSUM
// =============================
$page = [
    "title" => "BMW F10 / БМВ Ф10",
    "description" => "BMW F10 / БМВ Ф10",
    "keywords" => "БМВ 5 серии в наличии, BMW 5 Series Sedan в наличии, F10, Москва, купить, автомобиль, цена, фото, комплектации",
    "mail" => "info@autoimage.com",
    "phone" => "8 800 555 0000",
    "head_one" => "BMW F10 / БМВ Ф10",
    "head_two" => "Второй заголовок",
    "head_three" => "Третий заголовок",
    "footer" => "© 2018 год. BMW F10",
];
$pageimg = [
    "bg_top_src" => "media/slides/2.jpg",
    "slider_folder" => "img/img-product/",
    // alt title
    "img_bottom_left_src" => "./img/img-page/1.jpg",
    "img_bottom_right_src" => "./img/img-page/2.jpg",
    "img_bottom_left_alt" => "салон БМВ ф10",
    "img_bottom_right_alt" => "салон БМВ ф10",

];

$pagetxt = [
    "first_text_one" => "Текст dia/slides/2.jpg",
    "first_text_two" => "Текст img/img-page/1.jpg",
    "first_text_three" => "Текст img/img-page/2.jpg",
    "second_text_one" => "Текст dia/slides/2.jpg",
    "second_text_two" => "Текст img/img-page/1.jpg",
    "second_text_three" => "Текст img/img-page/2.jpg",
];
// $page['img_slider_folder']
?>


<!DOCTYPE HTML>
<!--[if IE 7 ]><html class="ie ie7 lte9 lte8 lte7" lang="en-US"><![endif]-->
<!--[if IE 8]><html class="ie ie8 lte9 lte8" lang="en-US">  <![endif]-->
<!--[if IE 9]><html class="ie ie9 lte9" lang="en-US"><![endif]-->

<html class="noIE">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title><?=$page['title']?></title>
<meta name="description" content="<?=$page['description']?>">
<meta name="keywords" content="<?=$page['keywords']?>">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<!-- Favorite Icons -->
<link rel="icon" href="img/favicon/favicon-32x32.png" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/favicon/favicon-144x144.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/favicon/favicon-72x72.png">
<link rel="apple-touch-icon-precomposed" href="img/favicon/favicon-54x54.png">
<!-- // Favorite Icons -->

<!-- FONT  -->
<link href='http://fonts.googleapis.com/css?family=Play:400,700&amp;subset=latin,greek-ext,greek,latin-ext,cyrillic,cyrillic-ext' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="font/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="font/icomoon/style.css" type="text/css" media="screen" />
<link rel="stylesheet" href="font/simple/simple-line-icons.css">
<link rel="stylesheet" href="font/autoicon/css/fontello.css">

<!--  /* Chrome hack: SVG is rendered more smooth in Windozze. 100% magic, uncomment if you need it. */
/* Note, that will break hinting! In other OS-es font will be not as sharp as it could be */  -->
<link rel="stylesheet" href="font/play/stylesheet.css">

<!-- // FONT -->

<!-- CSS -->
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/bootstrap-theme.css">
<link rel="stylesheet" href="css/animate.css">
<link rel="stylesheet" href="css/isotope.css">
<link rel="stylesheet" href="css/prettyphoto/default.css">
<link rel="stylesheet" href="css/mobilemenu.css">
<link rel="stylesheet" href="css/theme.css">
<link rel="stylesheet" href="css/hover-min.css">
<link rel="stylesheet" href="css/responsive.css">
<link rel="stylesheet" href="css/innerpage.css">
<link rel="stylesheet" href="css/flexslider-page.css"  />
<link rel='stylesheet prefetch' href='css/jquery.fancybox.min.css'>


<!--[if IE 8]>
		<script src="js/respond.min.js"></script>
		<script src="js/selectivizr-min.js"></script>
		<script src="js/PIE.min.js"></script>
<![endif]-->

<script>window.jQuery || document.write('<script src="js/jquery.min.js"><\/script>');</script>
<script src="js/modernizr.min.js"></script>
</head>
<body class="onepage sticky-header">
<div id="loading-mask">
  <div class="loading-img"><img alt="img" src="img/preloader.gif"  /></div>
</div>
<div class="top-bar">
  <div class="container">
    <div class="row">
      <div class="col-lg-7 col-md-7 col-sm-7 pull-left">
        <p><a href="tel:<?=$page['phone']?>">Телефон: <?=$page['phone']?></a>&nbsp;&nbsp; |&nbsp;&nbsp; <a href="mailto:<?=$page['mail']?>"><?=$page['mail']?></a> </p>
      </div>
      <div class="col-lg-5 col-md-5 col-sm-5 pull-right text-right">
        <div class="social"> <a href="#"><i class="fa fa-facebook"></i></a> <a href="#"><i class="fa fa-twitter"></i></a> <a href="#"><i class="fa fa-html5"></i></a> <a href="#"><i class="fa fa-youtube"></i></a> <a href="#"><i class="fa fa-flickr"></i></a> <a href="#"><i class="fa fa-vk"></i></a> </div>
      </div>
    </div>
  </div>
</div>

<!-- PAGE HEADER -->
<header class="page-header" role="banner">
  <div class="container">
    <div class="row">
      <div class="col-lg-3 col-md-3 col-sm-12">
        <div class="logo " > <a href="index.html#home" title="AutoImage - Single Page Responsive HTML Template"> <img  src="img/logo.png" alt="AutoImage - Single Page Responsive HTML Template" /> </a> </div>
      </div>
      <div class="col-sm-12 col-md-9 col-lg-9">
        <nav class="main-nav clearfix" role="navigation">
          <ul id="main-menu" class="menu main-menu hidden-xs">
            <li class="active"><a href="index.php">Главная</a></li>
            <li><a href="index.php#Services">Характеристики</a></li>
            <li><a href="index.php#our-team">Двигатели</a></li>
            <li><a href="index.php#our-reviews">Отзывы</a></li>
            <li><a href="index.php#about">Обзор</a></li>
            <li><a href="index.php#photo">Фото</a></li>
          </ul>
          <div id="mobile-menu" class="visible-xs">
            <div id="dl-menu" class="dl-menuwrapper menu">
              <button class="dl-trigger" title="Toggle Site Menu"><i class="icomoon-reorder"></i></button>
              <ul class="dl-menu">
                <li class="active"><a href="index.php#home">Главная</a></li>
                <li><a href="index.php#Services">Характеристики</a></li>
                <li><a href="index.php#our-team">Двигатели</a></li>
                <li><a href="index.php#our-reviews">Отзывы</a></li>
                <li><a href="index.php#about">Обзор</a></li>
                <li><a href="index.php#photo">Фото</a></li>
              </ul>
            </div>
            <!-- /dl-menuwrapper -->
          </div>
        </nav>
      </div>
    </div>
  </div>
</header>
<!-- //PAGE HEADER -->

<!-- TEXT SECTION -->
<!-- $page_img_top -->
<section class="section section-white page-text-header" style="background: url('<?=$pageimg['bg_top_src']?>') 100% 100% no-repeat; background-size: cover; background-position:center; height: 600px;">
  <div class="container">
    <div class="row">
      <div class="col-md-offset-3 col-md-6">
        <header class="section-header ">
          <div class="heading-wrap">
            <!-- $title -->
            <h1><?=$page['head_one']?></h1>
          </div>
        </header>
      </div>
    </div>
  </div>
</section>
<!-- //TEXT SECTION -->

<!-- TEXT SECTION -->
<section id="page-text-one" class="section section-white" >
  <div class="container">
    <div class="row">
      <div class="col-md-offset-3 col-md-6">
        <header class="section-header ">
          <div class=" animated" data-animation="bounceInLeft"> <i class="autologo_avto"></i> </div>
          <div class="heading-wrap">
            <!-- $page_head_two -->
            <h2 class="heading" ><span><?=$page['head_two']?></span></h2>
          </div>
        </header>
      </div>
    </div>
    <div class="row animated service-item"  data-animation="fadeInUp">
      <div class="col-lg-12 col-md-12 col-sm-12">
        <article class=" clearfix animated page-text"  data-animation="fadeInUp">
          <!-- <i class="auto1"></i> -->
          <p><?=$pagetxt['first_text_one']?>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ratione a nemo neque. Quibusdam necessitatibus eos obcaecati minima officiis, ipsa nihil libero eveniet perferendis enim, autem iure est aliquam consectetur esse. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ratione a nemo neque. Quibusdam necessitatibus eos obcaecati minima officiis, ipsa nihil libero eveniet perferendis enim, autem iure est aliquam consectetur esse. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ratione a nemo neque. Quibusdam necessitatibus eos obcaecati minima officiis, ipsa nihil libero eveniet perferendis enim, autem iure est aliquam consectetur esse.</p>
          <p><?=$pagetxt['first_text_two']?>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ratione a nemo neque. Quibusdam necessitatibus eos obcaecati minima officiis, ipsa nihil libero eveniet perferendis enim, autem iure est aliquam consectetur esse. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ratione a nemo neque. Quibusdam necessitatibus eos obcaecati minima officiis, ipsa nihil libero eveniet perferendis enim, autem iure est aliquam consectetur esse. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ratione a nemo neque. Quibusdam necessitatibus eos obcaecati minima officiis, ipsa nihil libero eveniet perferendis enim, autem iure est aliquam consectetur esse. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ratione a nemo neque. Quibusdam necessitatibus eos obcaecati minima officiis, ipsa nihil libero eveniet perferendis enim, autem iure est aliquam consectetur esse. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ratione a nemo neque. Quibusdam necessitatibus eos obcaecati minima officiis, ipsa nihil libero eveniet perferendis enim, autem iure est aliquam consectetur esse. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ratione a nemo neque. Quibusdam necessitatibus eos obcaecati minima officiis, ipsa nihil libero eveniet perferendis enim, autem iure est aliquam consectetur esse.</p>
          <p><?=$pagetxt['first_text_three']?>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ratione a nemo neque. Quibusdam necessitatibus eos obcaecati minima officiis, ipsa nihil libero eveniet perferendis enim, autem iure est aliquam consectetur esse. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ratione a nemo neque. Quibusdam necessitatibus eos obcaecati minima officiis, ipsa nihil libero eveniet perferendis enim, autem iure est   consectetur esse. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ratione a nemo neque. Quibusdam necessitatibus eos obcaecati minima officiis, ipsa nihil libero eveniet perferendis enim, autem iure est aliquam consectetur esse. Lorem ipsum, consectetur adipisicing elit. Ratione a nemo neque. Quibusdam necessitatibus eos obcaecati minima officiis, ipsa nihil libero eveniet perferendis enim, autem iure est aliquam consectetur esse. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ratione a nemo neque. Quibusdam necessitatibus eos obcaecati minima officiis, ipsa nihil libero eveniet perferendis enim,  iure est aliquam consectetur esse. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ratione a nemo neque. Quibusdam necessitatibus eos obcaecati minima officiis, ipsa nihil libero eveniet perferendis enim, autem iure est aliquam consectetur esse. Lorem ipsum dolor sit amet, consectetur adipisicing elit. a nemo neque. Quibusdam necessitatibus eos obcaecati minima officiis, ipsa nihil  eveniet perferendis enim, autem iure est aliquam consectetur esse. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ratione a nemo neque. Quibusdam necessitatibus eos obcaecati minima officiis, ipsa nihil libero eveniet perferendis enim, autem iure est aliquam consectetur esse. ipsum dolor sit amet, consectetur adipisicing elit. Ratione a nemo neque. Quibusdam necessitatibus eos obcaecati minima officiis, ipsa nihil libero eveniet perferendis enim, autem iure est aliquam consectetur esse evenieteveniet.  </p>
        </article>
      </div>
    </div>
  </div>
</section>
<!-- //TEXT SECTION -->

<!-- HOME SLIDER -->
<section id="home" class="section" >
  <div class="flexslider-page carousel">
    <ul class="slides">
    <?
    $directory = $pageimg['slider_folder']; // Папка с изображениями
    $thumb_directory =  "./img/thumb"; // Папка для миниатюр
    $allowed_types=array("jpg", "png", "gif");  //разрешеные типы изображений
    $file_parts = array();
    $ext="";
    $title="";
    $i=0;
    $dir_handle = @opendir($directory) or die("Ошибка при открытии папки !!!");
      while ($file = readdir($dir_handle)){

          if($file=="." || $file == "..") continue;  //пропустить ссылки на другие папки
          $file_parts = explode(".",$file);          //разделить имя файла и поместить его в массив
          $ext = strtolower(array_pop($file_parts));   //последний элеменет - это расширение

          if(in_array($ext,$allowed_types)){
            echo '<li style="background: url('.$directory.''.$file.') 100% 100% no-repeat; background-size: cover; background-position:center;"></li>';
            $i++;
          }

        }
    closedir($dir_handle);


    ?>
    </ul>
  </div>

  <!-- FlexSlider -->
  <script defer src="js/jquery.flexslider.js"></script>
  <script type="text/javascript">
    $(window).load(function() {
      $('.flexslider-page').flexslider({
        animation: "slide",
        animationLoop: false,
        itemWidth: 210,
        itemMargin: 5,
        minItems: 2,
        maxItems: 4
      });
    });


  </script>
</section>
<!-- // HOME SLIDER -->

<!-- TEXT SECTION -->
<section id="page-text-two" class="section section-white" >
  <div class="container">
    <div class="row">
      <div class="col-md-offset-3 col-md-6">
        <header class="section-header ">
          <div class=" animated" data-animation="bounceInLeft"> <i class="autologo_avto"></i> </div>
          <div class="heading-wrap">
            <h2 class="heading" ><span><?=$page['head_three']?></span></h2>
          </div>
        </header>
      </div>
    </div>
    <div class="row animated service-item"  data-animation="fadeInUp">
      <div class="col-lg-12 col-md-12 col-sm-12">
        <article class=" clearfix animated page-text"  data-animation="fadeInUp">
          <p><?=$pagetxt['second_text_one']?>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ratione a nemo neque. Quibusdam necessitatibus eos obcaecati minima officiis, ipsa nihil libero eveniet perferendis enim, autem iure est aliquam consectetur esse. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ratione a nemo neque. Quibusdam necessitatibus eos obcaecati minima officiis, ipsa nihil libero eveniet perferendis enim, autem iure est aliquam consectetur esse. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ratione a nemo neque. Quibusdam necessitatibus eos obcaecati minima officiis, ipsa nihil libero eveniet perferendis enim, autem iure est aliquam consectetur esse.</p>
          <p><?=$pagetxt['second_text_two']?>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ratione a nemo neque. Quibusdam necessitatibus eos obcaecati minima officiis, ipsa nihil libero eveniet perferendis enim, autem iure est aliquam consectetur esse. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ratione a nemo neque. Quibusdam necessitatibus eos obcaecati minima officiis, ipsa nihil libero eveniet perferendis enim, autem iure est aliquam consectetur esse. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ratione a nemo neque. Quibusdam necessitatibus eos obcaecati minima officiis, ipsa nihil libero eveniet perferendis enim, autem iure est aliquam consectetur esse. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ratione a nemo neque. Quibusdam necessitatibus eos obcaecati minima officiis, ipsa nihil libero eveniet perferendis enim, autem iure est aliquam consectetur esse. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ratione a nemo neque. Quibusdam necessitatibus eos obcaecati minima officiis, ipsa nihil libero eveniet perferendis enim, autem iure est aliquam consectetur esse. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ratione a nemo neque. Quibusdam necessitatibus eos obcaecati minima officiis, ipsa nihil libero eveniet perferendis enim, autem iure est aliquam consectetur esse.</p>
          <p><?=$pagetxt['second_text_three']?>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ratione a nemo neque. Quibusdam necessitatibus eos obcaecati minima officiis, ipsa nihil libero eveniet perferendis enim, autem iure est aliquam consectetur esse. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ratione a nemo neque. Quibusdam necessitatibus eos obcaecati minima officiis, ipsa nihil libero eveniet perferendis enim, autem iure est   consectetur esse. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ratione a nemo neque. Quibusdam necessitatibus eos obcaecati minima officiis, ipsa nihil libero eveniet perferendis enim, autem iure est aliquam consectetur esse. Lorem ipsum, consectetur adipisicing elit. Ratione a nemo neque. Quibusdam necessitatibus eos obcaecati minima officiis, ipsa nihil libero eveniet perferendis enim, autem iure est aliquam consectetur esse. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ratione a nemo neque. Quibusdam necessitatibus eos obcaecati minima officiis, ipsa nihil libero eveniet perferendis enim,  iure est aliquam consectetur esse. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ratione a nemo neque. Quibusdam necessitatibus eos obcaecati minima officiis, ipsa nihil libero eveniet perferendis enim, autem iure est aliquam consectetur esse. Lorem ipsum dolor sit amet, consectetur adipisicing elit. a nemo neque. Quibusdam necessitatibus eos obcaecati minima officiis, ipsa nihil  eveniet perferendis enim, autem iure est aliquam consectetur esse. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ratione a nemo neque. Quibusdam necessitatibus eos obcaecati minima officiis, ipsa nihil libero eveniet perferendis enim, autem iure est aliquam consectetur esse. ipsum dolor sit amet, consectetur adipisicing elit. Ratione a nemo neque. Quibusdam necessitatibus eos obcaecati minima officiis, ipsa nihil libero eveniet perferendis enim, autem iure est aliquam consectetur esse evenieteveniet.  </p>
        </article>
      </div>
    </div>
  </div>
</section>
<!-- //TEXT SECTION -->

<section class="section page-photo section-white" >
  <div class="container">
    <div class="row animated service-item"  data-animation="fadeInUp">
      <div class="col-lg-6 col-md-6 col-sm-6">
        <img class="img-responsive" src="<?=$pageimg['img_bottom_left_src']?>" alt="<?=$pageimg['img_bottom_left_alt']?>" title="<?=$pageimg['img_bottom_left_alt']?>">
      </div>
      <div class="col-lg-6 col-md-6 col-sm-6">
        <img class="img-responsive" src="<?=$pageimg['img_bottom_right_src']?>" alt="<?=$pageimg['img_bottom_right_alt']?>" title="<?=$pageimg['img_bottom_left_alt']?>">        
      </div>
    </div>
  </div>
</section>

<!-- PAGE FOOTER -->
<div class="section-footer">
  <div class="container">
    <footer class="page-footer">
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
          <div class="copyright"><?=$page['footer']?></div>
          <a class="go-top"><i class="fa fa-angle-double-up"></i></a> </div>
      </div>
    </footer>
  </div>
</div>

<!-- // PAGE FOOTER -->

<!-- Javascripts -->
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.isotope.min.js"></script>
<script src="js/jquery.prettyPhoto.min.js"></script>
<script src="js/jflickrfeed.min.js"></script>
<script src="js/sly.min.js"></script>
<script src="js/package.min.js"></script>
<script src="js/jquery.nav.js"></script>
<script src="js/scripts.js"></script>
<script src='js/jquery.fancybox.min.js'></script>
<script>
$(function() {
$("[data-fancybox]").fancybox();
});
</script>
</body>
</html>




