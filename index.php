<?php 

$home = [
    "title" => "BMW F10 / БМВ Ф10",
    "description" => "BMW F10 / БМВ Ф10",
    "keywords" => "БМВ 5 серии в наличии, BMW 5 Series Sedan в наличии, F10, Москва, купить, автомобиль, цена, фото, комплектации",
    "mail" => "info@autoimage.com",
    "phone" => "8 800 555 0000",
    "head_one" => "BMW F10 / БМВ Ф10",
    "head_two" => "Второй заголовок",
    "head_three" => "Третий заголовок",
    "footer" => "© 2018 год. BMW F10",
  ""  
];
$link = [
    'page_kuzov.php',
    'page_salon.php',
    'page_transmissiya.php',
    // 'templates/pages/page_kuzov.php',
    // 'templates/pages/page_salon.php',
    // 'templates/pages/page_transmissiya.php',
];
?>

<!DOCTYPE HTML>
<!--[if IE 7 ]><html class="ie ie7 lte9 lte8 lte7" lang="en-US"><![endif]-->
<!--[if IE 8]><html class="ie ie8 lte9 lte8" lang="en-US">	<![endif]-->
<!--[if IE 9]><html class="ie ie9 lte9" lang="en-US"><![endif]-->

<html class="noIE">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title><?=$home['title']?></title>
<meta name="description" content="<?=$home['description']?>">
<meta name="keywords" content="<?=$home['keywords']?>">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<!-- Favorite Icons -->
<link rel="icon" href="img/favicon/favicon-32x32.png" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/favicon/favicon-144x144.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/favicon/favicon-72x72.png">
<link rel="apple-touch-icon-precomposed" href="img/favicon/favicon-54x54.png">
<!-- // Favorite Icons -->

<!-- FONT  -->
<link href='http://fonts.googleapis.com/css?family=Play:400,700&amp;subset=latin,greek-ext,greek,latin-ext,cyrillic,cyrillic-ext' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="font/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="font/icomoon/style.css" type="text/css" media="screen" />
<link rel="stylesheet" href="font/simple/simple-line-icons.css">
<link rel="stylesheet" href="font/autoicon/css/fontello.css">

<!--  /* Chrome hack: SVG is rendered more smooth in Windozze. 100% magic, uncomment if you need it. */
/* Note, that will break hinting! In other OS-es font will be not as sharp as it could be */  -->
<link rel="stylesheet" href="font/play/stylesheet.css">

<!-- // FONT -->

<!-- CSS -->
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/bootstrap-theme.css">
<link rel="stylesheet" href="css/animate.css">
<link rel="stylesheet" href="css/isotope.css">
<link rel="stylesheet" href="css/prettyphoto/default.css">
<link rel="stylesheet" href="css/mobilemenu.css">
<link rel="stylesheet" href="css/theme.css">
<link rel="stylesheet" href="css/hover-min.css">
<link rel="stylesheet" href="css/responsive.css">
<link rel="stylesheet" href="css/innerpage.css">
<!-- ! PAGE flexslider-page.css -->
<link rel="stylesheet" href="css/flexslider.css"  />
<link rel='stylesheet prefetch' href='css/jquery.fancybox.min.css'>


<!--[if IE 8]>
		<script src="js/respond.min.js"></script>
		<script src="js/selectivizr-min.js"></script>
		<script src="js/PIE.min.js"></script>
<![endif]-->

<script>window.jQuery || document.write('<script src="js/jquery.min.js"><\/script>');</script>
<script src="js/modernizr.min.js"></script>
</head>
<body class="onepage sticky-header">
<div id="loading-mask">
  <div class="loading-img"><img alt="img" src="img/preloader.gif"  /></div>
</div>
<div class="top-bar">
  <div class="container">
    <div class="row">
      <div class="col-lg-7 col-md-7 col-sm-7 pull-left">
        <p><a href="tel:<?=$home['phone']?>">Телефон: <?=$home['phone']?></a>&nbsp;&nbsp; |&nbsp;&nbsp; <a href="mailto:<?=$page['mail']?>"><?=$page['mail']?></a> </p>
      </div>
      <div class="col-lg-5 col-md-5 col-sm-5 pull-right text-right">
        <div class="social"> <a href="#"><i class="fa fa-facebook"></i></a> <a href="#"><i class="fa fa-twitter"></i></a> <a href="#"><i class="fa fa-html5"></i></a> <a href="#"><i class="fa fa-youtube"></i></a> <a href="#"><i class="fa fa-flickr"></i></a> <a href="#"><i class="fa fa-vk"></i></a> </div>
      </div>
    </div>
  </div>
</div>

<!-- PAGE HEADER -->
<header class="page-header" role="banner">
  <div class="container">
    <div class="row">
      <div class="col-lg-3 col-md-3 col-sm-12">
        <div class="logo " > <a href="index.html#home" title="AutoImage - Single Page Responsive HTML Template"> <img  src="img/logo.png" alt="AutoImage - Single Page Responsive HTML Template" /> </a> </div>
      </div>
      <div class="col-sm-12 col-md-9 col-lg-9">
        <nav class="main-nav clearfix" role="navigation">
          <ul id="main-menu" class="menu main-menu hidden-xs">
            <li class="active"><a href="index.php#home">Главная</a></li>
            <li><a href="index.php#Services">Характеристики</a></li>
            <li><a href="index.php#our-team">Двигатели</a></li>
            <li><a href="index.php#photo">Фото</a></li>
            <li><a href="index.php#our-reviews">Отзывы</a></li>
            <li><a href="index.php#about">Обзор</a></li>
          </ul>
          <div id="mobile-menu" class="visible-xs">
            <div id="dl-menu" class="dl-menuwrapper menu">
              <button class="dl-trigger" title="Toggle Site Menu"><i class="icomoon-reorder"></i></button>
              <ul class="dl-menu">
                <li class="active"><a href="index.php#home">Главная</a></li>
                <li><a href="index.php#Services">Характеристики</a></li>
                <li><a href="index.php#our-team">Двигатели</a></li>
                <li><a href="index.php#photo">Фото</a></li>
                <li><a href="index.php#our-reviews">Отзывы</a></li>
                <li><a href="index.php#about">Обзор</a></li>
              </ul>
            </div>
            <!-- /dl-menuwrapper -->
          </div>
        </nav>
      </div>
    </div>
  </div>
</header>
<!-- //PAGE HEADER -->

<!-- HOME SLIDER -->
<section id="home" class="section  home-slider" >
  <div class="flex-slider">
    <ul class="slides">
        <li>
          <img src="media/slides/2.jpg" alt="img" />
          <div class="flex-caption">
            <h1>BMW F10 / БМВ Ф10</h1>
            <p> Phasellus imperdiet libero sit amet ante. Donec convallis elementum leo. Cum  Viva mus lobortis consequat purus. Curabitur commodo. Etiam sed nulla</p>
            <a href="/" class="btn btn-main btn-primary btn-lg uppercase" ><span>View Cars</span></a> 
          </div>
        </li>
    </ul>
  </div>
  <!-- FlexSlider -->
  <script defer src="js/jquery.flexslider.js"></script>
  <script type="text/javascript">
    $(window).load(function(){
  		$('.flexslider').flexslider({
   			animation: 'fade', 		   //String: Select your animation type, "fade" or "slide"
   			controlNav: true,   	  //Boolean: Create navigation for paging control of each clide? Note: Leave true for manualControls usage
   			directionNav: true,		 //Boolean: Create navigation for previous/next navigation? (true/false)
   			slideshowSpeed: 7000,   //Integer: Set the speed of the slideshow cycling, in milliseconds
   			animationSpeed: 600,   //Integer: Set the speed of animations, in milliseconds
   			pauseOnHover: false,   //Boolean: Pause the slideshow when hovering over slider, then resume when no longer hovering
   			prevText: "",        //String: Set the text for the "previous" directionNav item
   			nextText: ""		//String: Set the text for the "next" directionNav item
   		});
    });
  </script>
</section>
<!-- // HOME SLIDER -->

<!-- SERVICES SECTION -->
<section id="Services" class="section section-white" >
  <div class="container">
    <div class="row">
      <div class="col-md-offset-3 col-md-6">
        <header class="section-header ">
          <div class=" animated" data-animation="bounceInLeft"> <i class="autologo_avto"></i> </div>
          <div class="heading-wrap">
            <h2 class="heading" ><span> Общие характеристики</span></h2>
          </div>
        </header>
      </div>
    </div>
    <div class="row animated service-item"  data-animation="fadeInUp">
      <!-- BEGIN -->
      <div class="col-lg-3 col-md-3 col-sm-3">
        <article class=" clearfix animated text-center"  data-animation="fadeInUp">
          <!-- <i class="auto1"></i> -->
          <h4>Экстерьер</h4>
          <p>Phasellus imperdiet libero sit amet ante. Donec convallis elementum leo. Cum muslobortis consequat purus.</p>
        </article>
      </div>
      <div class="col-lg-3 col-md-3 col-sm-3">
        <article class=" clearfix animated text-center"  data-animation="fadeInUp">
          <!-- <i class="auto2"></i> -->
          <h4>Интерьер</h4>
          <p>Phasellus imperdiet libero sit amet ante. Donec convallis elementum leo. Cum muslobortis consequat purus.</p>
        </article>
      </div>
      <div class="col-lg-3 col-md-3 col-sm-3">
        <article class=" clearfix animated text-center"  data-animation="fadeInUp">
          <!-- <i class="auto3"></i> -->
          <h4>Мультимедиа</h4>
          <p>Phasellus imperdiet libero sit amet ante. Donec convallis elementum leo. Cum muslobortis consequat purus.</p>
        </article>
      </div>
      <div class="col-lg-3 col-md-3 col-sm-3">
        <article class=" clearfix animated text-center"  data-animation="fadeInUp">
          <!-- <i class="auto4"></i> -->
          <h4>Безопасность</h4>
          <p>Phasellus imperdiet libero sit amet ante. Donec convallis elementum leo. Cum muslobortis consequat purus.</p>
        </article>
      </div>

      <!-- Tab panes -->

      <!-- END -->
    </div>
  </div>
</section>

<section id="Services2" >

<div class="bg" style="background-image:url(media/services/two.jpg);"></div>

  <div class="container list-service" >
    <div class="row ">

      <!-- BEGIN -->
      <div class="col-lg-3 col-md-3 col-sm-3">
        <article class=" clearfix  text-left" >

          <!-- Nav tabs -->
          <ul class="nav nav-tabs">
            <!-- <li class="active"><i class="auto5"></i><a href="#tab1-1" data-toggle="tab">Кузов</a></li>
            <li><i class="auto5"></i><a href="#tab1-2" data-toggle="tab">Салон</a></li>
            <li ><i class="auto6"></i><a href="#tab1-3" data-toggle="tab"> Трансмиссия</a></li> -->

            <li class="active"><a href="#tab1-1" data-toggle="tab">Экстерьер</a></li>
            <li><a href="#tab1-2" data-toggle="tab">Салон</a></li>
            <li><a href="#tab1-3" data-toggle="tab">Характеристики</a></li>
            <!-- <li><i class="auto7"></i><a href="#tab1-4" data-toggle="tab">Электроника</a></li> -->
          </ul>
        </article>
      </div>

      <!-- Tab panes -->
      <div class="tab-content ">
        <div class="tab-pane active" id="tab1-1">
          <div class="col-xs-12 col-sm-5 col-lg-6 ">
            <article class="clearfix  text-left" >
              <h3>AUDIO SYATEMS</h3>
              <p>Phasellus imperdiet libero sit amet ante. Donec convallis elementum leo. Cum  Vivamus
                lobo rtis consequat purus. Curabitur commodo. Eltiam sed nulla. Lorem ipsum dolor sit
                met conse clt etuer adipiscing elit. Nulla libero. </p>
              <p>Donec convallis elementum leo. Duis ald ipiscing nulla elt metus. Donec gravida tortora massa. Fusce fermentum neque a rutrum varius odio. </p>
              <a href="<?=$link['0']?>" class="btn btn-main btn-primary btn-lg uppercase"><span>подробнее</span></a>
            </article>
          </div>
          <div class="col-xs-12 col-sm-4 col-lg-3">
            <article class="clearfix  text-left" >
              <h4>Преимущества</h4>
              <hr>
              <ul class="check-list">
                <li><i class="fa fa-check"></i>Etiam ante lectus, venenatis at </li>
                <li><i class="fa fa-check"></i>Maecenas commodo ed soll</li>
                <li><i class="fa fa-check"></i>Integer mollis ullam corper metus proin </li>
                <li><i class="fa fa-check"></i> Etiam ante lectus, venenatis at</li>
                <li><i class="fa fa-check"></i> Maecenas commodo ed soll</li>
              </ul>
            </article>
          </div>
        </div>
        <div class="tab-pane " id="tab1-2">
          <div class="col-xs-12 col-sm-5 col-lg-6 ">
            <article class="clearfix text-left" >  
              <h3>Creative</h3>
              <p>Phasellus imperdiet libero sit amet ante. Donec convallis elementum leo. Cum  Vivamus
                lobo rtis consequat purus. Curabitur commodo. Eltiam sed nulla. Lorem ipsum dolor sit
                met conse clt etuer adipiscing elit. Nulla libero. </p>
              <p>Donec convallis elementum leo. Duis ald ipiscing nulla elt metus. Donec gravida tortora massa. Fusce fermentum neque a rutrum varius odio. </p>
              <a href="<?=$link['1']?>" class="btn btn-main btn-primary btn-lg uppercase"><span>подробнее</span></a>              
            </article>
          </div>
          <div class="col-xs-12 col-sm-4 col-lg-3">
            <article class="clearfix text-left" >
              <h4>Преимущества</h4>
              <hr>
              <ul class="check-list">
                <li><i class="fa fa-check"></i>Integer mollis ullam corper metus proin </li>
                <li><i class="fa fa-check"></i> Maecenas commodo ed soll</li>
                <li><i class="fa fa-check"></i>Etiam ante lectus, venenatis at </li>
                <li><i class="fa fa-check"></i> Etiam ante lectus, venenatis at</li>
                <li><i class="fa fa-check"></i>Maecenas commodo ed soll</li>
              </ul>
            </article>
          </div>
        </div>
        <div class="tab-pane " id="tab1-3">
          <div class="col-xs-12 col-sm-5 col-lg-6 ">
            <article class="clearfix text-left" >
              <h3>Creative</h3>
              <p>Phasellus imperdiet libero sit amet ante. Donec convallis elementum leo. Cum  Vivamus
                lobo rtis consequat purus. Curabitur commodo. Eltiam sed nulla. Lorem ipsum dolor sit
                met conse clt etuer adipiscing elit. Nulla libero. </p>
              <p>Donec convallis elementum leo. Duis ald ipiscing nulla elt metus. Donec gravida tortora massa. Fusce fermentum neque a rutrum varius odio. </p>
              <a href="<?=$link['2']?>" class="btn btn-main btn-primary btn-lg uppercase"><span>подробнее</span></a>
            </article>
          </div>
          <div class="col-xs-12 col-sm-4 col-lg-3">
            <article class="clearfix text-left" >
              <h4>Преимущества</h4>
              <hr>
              <ul class="check-list">
                <li><i class="fa fa-check"></i>Integer mollis ullam corper metus proin </li>
                <li><i class="fa fa-check"></i> Maecenas commodo ed soll</li>
                <li><i class="fa fa-check"></i>Etiam ante lectus, venenatis at </li>
                <li><i class="fa fa-check"></i> Etiam ante lectus, venenatis at</li>
                <li><i class="fa fa-check"></i>Maecenas commodo ed soll</li>
              </ul>
            </article>
          </div>
        </div>
      </div>
      <!-- END -->
    </div>
  </div>
</section>

<!-- // SERVICES SECTION -->

<!-- OUR TEAM SECTION -->
<section id="our-team" class="section section-white" >
  <div class="container">
    <div class="row">
      <div class="col-md-offset-2 col-md-8">
        <header class="section-header">
          <div class=" animated" data-animation="bounceInLeft"> <i class="autologo_avto"></i> </div>
          <div class="heading-wrap">
            <h2 class="heading " ><span>Двигатели</span></h2>
          </div>
        </header>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row text-center animated animation-done bounceInRight" data-animation="bounceInRight">
      <div class="team-frame" style="overflow: hidden;">
        <ul class="team-slider" style="transform: translateZ(0px) translateX(0px); width: 2416px;">
          <li class="active">
            <div class="team-member animated" data-animation="fadeIn">
              <div class="details-team">
                <h3 class="heading">523i</h3>
                <div class=""></div>
                <table>
                  <tr>
                    <th colspan="5">Характеристики</th>
                  </tr>
                  <tr>
                    <td>Объем двигателя, куб. см.:</td>
                    <td>2996</td>
                  </tr>
                  <tr>
                    <td>Объем двигателя, л.:</td>
                    <td>3.0</td>
                  </tr>
                  <tr>
                    <td>Клапанов на цилиндр:</td>
                    <td>4</td>
                  </tr>
                  <tr>
                    <td>Мощность, л.с.:</td>
                    <td>204</td>
                  </tr>
                  <tr>
                    <td>Крутящий момент, Нм/об. в мин.:</td>
                    <td>270/1500</td>
                  </tr>
                  <tr>
                    <td>Максимальная скорость, км/ч:</td>
                    <td>234</td>
                  </tr>
                </table>
              </div>
            </div>
          </li>
          <li>
            <div class="team-member animated" data-animation="fadeIn">
              <div class="details-team">
                <h3 class="heading">523i</h3>
                <div class=""></div>
                <table>
                  <tr>
                    <th colspan="5">Характеристики</th>
                  </tr>
                  <tr>
                    <td>Объем двигателя, куб. см.:</td>
                    <td>2996</td>
                  </tr>
                  <tr>
                    <td>Объем двигателя, л.:</td>
                    <td>3.0</td>
                  </tr>
                  <tr>
                    <td>Клапанов на цилиндр:</td>
                    <td>4</td>
                  </tr>
                  <tr>
                    <td>Мощность, л.с.:</td>
                    <td>204</td>
                  </tr>
                  <tr>
                    <td>Крутящий момент, Нм/об. в мин.:</td>
                    <td>270/1500</td>
                  </tr>
                  <tr>
                    <td>Максимальная скорость, км/ч:</td>
                    <td>234</td>
                  </tr>
                </table>
              </div>
            </div>
          </li>
          <li>
            <div class="team-member animated" data-animation="fadeIn">
              <div class="details-team">
                <h3 class="heading">523i</h3>
                <div class=""></div>
                <table>
                  <tr>
                    <th colspan="5">Характеристики</th>
                  </tr>
                  <tr>
                    <td>Объем двигателя, куб. см.:</td>
                    <td>2996</td>
                  </tr>
                  <tr>
                    <td>Объем двигателя, л.:</td>
                    <td>3.0</td>
                  </tr>
                  <tr>
                    <td>Клапанов на цилиндр:</td>
                    <td>4</td>
                  </tr>
                  <tr>
                    <td>Мощность, л.с.:</td>
                    <td>204</td>
                  </tr>
                  <tr>
                    <td>Крутящий момент, Нм/об. в мин.:</td>
                    <td>270/1500</td>
                  </tr>
                  <tr>
                    <td>Максимальная скорость, км/ч:</td>
                    <td>234</td>
                  </tr>
                </table>
              </div>
            </div>
          </li>
          <li>
            <div class="team-member animated" data-animation="fadeIn">
              <div class="details-team">
                <h3 class="heading">523i</h3>
                <div class=""></div>
                <table>
                  <tr>
                    <th colspan="5">Характеристики</th>
                  </tr>
                  <tr>
                    <td>Объем двигателя, куб. см.:</td>
                    <td>2996</td>
                  </tr>
                  <tr>
                    <td>Объем двигателя, л.:</td>
                    <td>3.0</td>
                  </tr>
                  <tr>
                    <td>Клапанов на цилиндр:</td>
                    <td>4</td>
                  </tr>
                  <tr>
                    <td>Мощность, л.с.:</td>
                    <td>204</td>
                  </tr>
                  <tr>
                    <td>Крутящий момент, Нм/об. в мин.:</td>
                    <td>270/1500</td>
                  </tr>
                  <tr>
                    <td>Максимальная скорость, км/ч:</td>
                    <td>234</td>
                  </tr>
                </table>
              </div>
            </div>
          </li>
          <li>
            <div class="team-member animated" data-animation="fadeIn">
              <div class="details-team">
                <h3 class="heading">523i</h3>
                <div class=""></div>
                <table>
                  <tr>
                    <th colspan="5">Характеристики</th>
                  </tr>
                  <tr>
                    <td>Объем двигателя, куб. см.:</td>
                    <td>2996</td>
                  </tr>
                  <tr>
                    <td>Объем двигателя, л.:</td>
                    <td>3.0</td>
                  </tr>
                  <tr>
                    <td>Клапанов на цилиндр:</td>
                    <td>4</td>
                  </tr>
                  <tr>
                    <td>Мощность, л.с.:</td>
                    <td>204</td>
                  </tr>
                  <tr>
                    <td>Крутящий момент, Нм/об. в мин.:</td>
                    <td>270/1500</td>
                  </tr>
                  <tr>
                    <td>Максимальная скорость, км/ч:</td>
                    <td>234</td>
                  </tr>
                </table>
              </div>
            </div>
          </li>
          <li>
            <div class="team-member animated" data-animation="fadeIn">
              <div class="details-team">
                <h3 class="heading">523i</h3>
                <div class=""></div>
                <table>
                  <tr>
                    <th colspan="5">Характеристики</th>
                  </tr>
                  <tr>
                    <td>Объем двигателя, куб. см.:</td>
                    <td>2996</td>
                  </tr>
                  <tr>
                    <td>Объем двигателя, л.:</td>
                    <td>3.0</td>
                  </tr>
                  <tr>
                    <td>Клапанов на цилиндр:</td>
                    <td>4</td>
                  </tr>
                  <tr>
                    <td>Мощность, л.с.:</td>
                    <td>204</td>
                  </tr>
                  <tr>
                    <td>Крутящий момент, Нм/об. в мин.:</td>
                    <td>270/1500</td>
                  </tr>
                  <tr>
                    <td>Максимальная скорость, км/ч:</td>
                    <td>234</td>
                  </tr>
                </table>
              </div>
            </div>
          </li>
        </ul>
      </div>
      <div class="portfolio-navigation"> <a href="javascript:void(0);" class="btn btn-primary slider-direction prev-page disabled"><i class="icomoon-arrow-left2"></i></a> <a href="javascript:void(0);" class="btn btn-primary slider-direction next-page"><i class="icomoon-arrow-right2"></i></a>
        <ul class="pages unstyled">
          <li class="active">1</li>
          <li>2</li>
          <li>3</li>
          <li>4</li>
        </ul>
      </div>
    </div>
  </div>
</section>
<!-- COMMON PARAMETERS -->
<section id="our-team" class="section section-white">
  <div class="container">
    <div class="row">
      <div class="col-md-offset-2 col-md-8">
        <header class="section-header">
          <div class="animated animation-done bounceInLeft" data-animation="bounceInLeft"> <i class="autologo_avto"></i> </div>
          <div class="heading-wrap">
            <h2 class="heading "><span>Общие характеристики</span></h2>
          </div>
        </header>
      </div>
    </div>
  </div>

  <div class="container">
    <div class="row animated animation-done bounceInRight" data-animation="bounceInRight">
      <table class="params-all">
        <tbody>
          <!-- <tr>
            <th colspan="2">Трансмиссия</th>
          </tr>
          <tr>
            <td class="table-param">Коробка передач</td>
            <td class="table-val">8АКПП</td>
          </tr> -->
          <tr>
            <th colspan="2">Подвеска</th>
          </tr>
          <tr>
            <td class="table-param">Тип передней подвески</td>
            <td class="table-val">независимая многорычажная</td>
          </tr>
          <tr>
            <td class="table-param">Тип задней подвески</td>
            <td class="table-val">независимая многорычажная</td>
          </tr>
          <tr>
            <th colspan="2">Тормозная система</th>
          </tr>
          <tr>
            <td class="table-param">Передние тормоза</td>
            <td class="table-val">дисковые вентилируемые</td>
          </tr>
          <tr>
            <td class="table-param">Задние тормоза</td>
            <td class="table-val">дисковые вентилируемые</td>
          </tr>
          <tr>
            <th colspan="2">Рулевое управление</th>
          </tr>
          <tr>
            <td class="table-param">Тип усилителя</td>
            <td class="table-val">электрический</td>
          </tr>
          <tr>
            <th colspan="2">Шины</th>
          </tr>
          <tr>
            <td class="table-param">Размер шин</td>
            <td class="table-val">225/55 R17 / 245/45 R18</td>
          </tr>
          <tr>
            <td class="table-param">Размер дисков</td>
            <td class="table-val">8.0Jx17 / 8.0Jx18</td>
          </tr>
          <tr>
            <th colspan="2">Топливо</th>
          </tr>
          <tr>
            <td class="table-param">Экологический класс</td>
            <td class="table-val">Евро-6</td>
          </tr>
          <tr>
            <td class="table-param">Объем бака, л</td>
            <td class="table-val">70</td>
          </tr>
          <tr>
            <th colspan="2">Габаритные размеры</th>
          </tr>
          <tr>
            <td class="table-param">Количество мест</td>
            <td class="table-val">5</td>
          </tr>
          <tr>
            <td class="table-param">Количество дверей</td>
            <td class="table-val">4</td>
          </tr>
          <tr>
            <td class="table-param">Длина, мм</td>
            <td class="table-val">4907</td>
          </tr>
          <tr>
            <td class="table-param">Высота, мм</td>
            <td class="table-val">1464</td>
          </tr>
          <tr>
            <td class="table-param">Колесная база, мм</td>
            <td class="table-val">2968</td>
          </tr>
          <tr>
            <td class="table-param">Колея передних колес, мм</td>
            <td class="table-val">1600</td>
          </tr>
          <tr>
            <td class="table-param">Колея задних колес, мм</td>
            <td class="table-val">1627</td>
          </tr>
          <tr>
            <td class="table-param">Передний свес, мм</td>
            <td class="table-val">832</td>
          </tr>
          <tr>
            <td class="table-param">Задний свес, мм</td>
            <td class="table-val">1107</td>
          </tr>
          <tr>
            <td class="table-param">Объем багажника (мин/макс), л</td>
            <td class="table-val">520</td>
          </tr>
          <tr>
            <td class="table-param">Дорожный просвет (клиренс), мм</td>
            <td class="table-val">141</td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</section>
<!-- // COMMON PARAMETERS -->
<!-- LATEST OFFERS -->


<section id="photo" class="section">
  <div class="container">
    <div class="row " >
      <div class="col-md-offset-3 col-md-6">
        <header class="section-header section-header-type2">
          <div class="heading-wrap">
            <h2 class="heading heading-type2" ><span>Фото</span></h2>
          </div>
          <div class="text-center">
            <a class="post-vk-link" href="https://vk.com/bmw5f10">Смотреть все фото</a>
          </div>
        </header>
      </div>
    </div>
  </div>
  <div class="row">
    <?php include 'templates/part/part_gallery.php';?>
  </div>
</section>

<section id="post_vk" class="section">
  <div class="container">
    <div class="row " >
      <div class="col-md-offset-3 col-md-6">
        <header class="section-header section-header-type2">
          <div class="heading-wrap">
            <h2 class="heading heading-type2" ><span>Новости группы ВК</span></h2>
          </div>
          <div class="text-center">
            <a class="post-vk-link" href="https://vk.com/bmw5f10">https://vk.com/bmw5f10</a>
          </div>
        </header>
      </div>
    </div>
  </div>
  <div class="row">
    <?php include 'templates/part/part_post_vk.php';?>
  </div>
</section>


<!-- REVIEWS SECTION-->
<section class="section section-white" id="our-reviews">
  <div class="container">
    <div class="row">
      <div class="col-md-offset-2 col-md-8">

         <header class="section-header">
          <div class=" animated" data-animation="bounceInLeft"> <i class="autologo_avto"></i> </div>
          <div class="heading-wrap">
            <h2 class="heading " ><span>Отзывы владельцев</span></h2>
          </div>
        </header>
      </div>
    </div>
  </div>
  <div class="container">
    <div  class="row text-center">
      <div class="reviews-frame " >
        <ul class="review-slider" >
          <li>
            <div data-animation="fadeIn" class="team-member animated  fadeIn">
              <div class="avatar-review"> <img src="media/team/1.png" alt="img"></div>
              <div class="details-review">
                <div class="desc-det">BMW 5 530d xDrive
                Автомобиль очень надежный , удобный , мощный , экономичный.
                За рулем этой машины чувствуешь себя уверенно и спокойно. На дороге стоит как вкопанная при этом агрессивность присутствует. 
                Вместительный багажник. задние сиденья складываются достаточно ровно. 
                </div>
                <div class="review-autor">
                  <h3 class="heading">Михаил Семенов</h3>
                  <h4 class="sub-heading">Владелец</h4>
                </div>
              </div>
            </div>
          </li>
          <li>
            <div data-animation="fadeIn" class="team-member animated  fadeIn">
              <div class="avatar-review"> <img src="media/team/2.png" alt="img"></div>
              <div class="details-review">
                <div class="desc-det"> BMW 5 525d xDrive
                Брал практически новую, езжу уже два года. Немец. Пока ни одной поломки, кроме перегоревшей лампочки в багажнике - как-то странно закреплена. Делал только плановые ТО, никаких проблем. Ездит как настоящий немецкий механизм без сбоев. Одно удовольствие. Качественная, серьезная машина.
                 </div>
                <div class="review-autor">
                  <h3 class="heading">Сергей Иванов</h3>
                  <h4 class="sub-heading">Владелец</h4>
                </div>
              </div>
            </div>
          </li>
          <li>
            <div data-animation="fadeIn" class="team-member animated  fadeIn">
              <div class="avatar-review"> <img src="media/team/3.png" alt="img"></div>
              <div class="details-review">
                <div class="desc-det"> BMW 5 530d
                Автомобиль покупался в 2010 году сразу после появления новой 5ки в салонах. Дизайн покорил меня и заставил влюбиться в данный автомобиль с первого взгляда. В мае будет 4 года с момента покупки, и ни одной минуты я не жалела о сделанном выборе. Очень динамичный, комфортный автомобиль. 
                 </div>
                <div class="review-autor">
                  <h3 class="heading">Ирина Савина</h3>
                  <h4 class="sub-heading">Владелец</h4>
                </div>
              </div>
            </div>
          </li>
          <li>
            <div data-animation="fadeIn" class="team-member animated  fadeIn">
              <div class="avatar-review"> <img src="media/team/4.png" alt="img"></div>
              <div class="details-review">
                <div class="desc-det"> BMW 5 520i 
                Хороший в управлении авто и как не странно с мягкой подвеской, что не скажешь о предыдущих моделях. После паркетника CRV не привычно сидеть внизу, но это быстро проходит. Машина оч. нравится.
                 </div>
                <div class="review-autor">
                  <h3 class="heading">Семен Глебов</h3>
                  <h4 class="sub-heading">Владелец</h4>
                </div>
              </div>
            </div>
          </li>
          <li>
            <div data-animation="fadeIn" class="team-member animated  fadeIn">
              <div class="avatar-review"> <img src="media/team/5.png" alt="img"></div>
              <div class="details-review">
                <div class="desc-det"> BMW 5 530d
                При эксплуатации ни с одной проблемой не столкнулась. Брала машину новую в салоне. Техобслуживание пока вообще не понадобилось. Единственное - покупала зимние шины тоже в салоне (шины с дисками, чтобы было удобнее менять) - так мне там резину и заменили. В общем проблем не было и нет с эксплуатацией.
                 </div>
                <div class="review-autor">
                  <h3 class="heading">Иван Акопян</h3>
                  <h4 class="sub-heading">Владелец</h4>
                </div>
              </div>
            </div>
          </li>
          <li>
            <div data-animation="fadeIn" class="team-member animated  fadeIn">
              <div class="avatar-review"> <img src="media/team/6.png" alt="img"></div>
              <div class="details-review">
                <div class="desc-det"> Покупал в Автодоме. Двиг. рядная шестерка 260л.с., регулируемая подвеска, комплектация практически полная, кроме ночного виденья и контроля полос, есть все. Авто доволен. Плавность хода, комфорт, шумка на высоте. Правда при переходе на режим комфорт случаются пробои подвески на средних с виду колдобинах.
                 </div>
                <div class="review-autor">
                  <h3 class="heading">Александр Баргович</h3>
                  <h4 class="sub-heading">Владелец</h4>
                </div>
              </div>
            </div>
          </li>
        </ul>
      </div>
      <div class="portfolio-navigation"> <a class="btn btn-primary slider-direction prev-page disabled" href="javascript:void(0);"><i class="icomoon-arrow-left2"></i></a> <a class="btn btn-primary slider-direction next-page" href="javascript:void(0);"><i class="icomoon-arrow-right2"></i></a>
        <ul class="pages unstyled">
          <li class="active">1</li>
          <li>2</li>
          <li>3</li>
          <li>4</li>
          <li>5</li>
          <li>6</li>
        </ul>
      </div>
    </div>
  </div>
</section>

<!-- ABOUT -->
<section class="section section-white" id="about" >
  <div class="container container-about" >
    <div class="row">
      <div class="col-md-offset-3 col-md-6">
        <header class="section-header">
          <div class=" animated" data-animation="bounceInLeft"> <i class="autologo_avto"></i> </div>
          <div class="heading-wrap">
            <h2 class="heading"><span>Обзор на автомобиль</span></h2>
          </div>
        </header>
      </div>
    </div>
    <div class="row animated" data-animation="bounceInRight">

      <!-- Tab panes -->
      <div class="tab-content video-reviews col-xs-12 col-sm-12 col-lg-12">
        <iframe width="100%" height="515" src="https://www.youtube.com/embed/Dye48JcD0pw" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
      </div>

      <!-- END -->
    </div>
  </div>
</section>

<!-- PAGE FOOTER -->
<div class="section-footer">
  <div class="container">
    <footer class="page-footer">
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
          <div class="copyright"> © 2018 год. BMW F10</div>
          <a class="go-top"><i class="fa fa-angle-double-up"></i></a> </div>
      </div>
    </footer>
  </div>
</div>

<!-- // PAGE FOOTER -->

<!-- Javascripts -->
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.isotope.min.js"></script>
<script src="js/jquery.prettyPhoto.min.js"></script>
<script src="js/jflickrfeed.min.js"></script>
<script src="js/sly.min.js"></script>
<script src="js/package.min.js"></script>
<script src="js/jquery.nav.js"></script>
<script src="js/scripts.js"></script>
<script src='js/jquery.fancybox.min.js'></script>
<script>
$(function() {
$("[data-fancybox]").fancybox();
});
</script>
</body>
</html>




